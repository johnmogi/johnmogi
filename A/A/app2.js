
// input.getValue("enter first name")
// .then(firstName => {
//     console.log("Your first name : " + firstName);

//     input.getValue("enter last name: ")
//     .then(lastName => {
//         console.log("Your last name : " + lastName);
//         input.close();
//     })
//     .catch(err => console.log(err.message));
// })
// .catch(err => console.log(err.message))

const input = require("./app")
async function main() {
    try {
        const firstName = await input.getValue("enter your first name :");
        const lastName = await input.getValue("enter your last name :");
        console.log("full name :" + firstName + " " + lastName)
    }
    catch (err) {
        console.log(err.message)
    }
    finally {
        input.close();

    }
}
main();