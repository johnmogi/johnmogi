const readline = require("readline");


// config:
const ui = readline.createInterface({
    input: process.stdin,
    output: process.stdout

});



function getValue(text) {
    return new Promise((resolve, reject) => {
        try {
            ui.question(text, value => resolve(value));
        }
        catch (err) {
            reject(err)
        }
    })
}

function close() {
    ui.close();
}
module.exports = {
    getValue,
    close
}