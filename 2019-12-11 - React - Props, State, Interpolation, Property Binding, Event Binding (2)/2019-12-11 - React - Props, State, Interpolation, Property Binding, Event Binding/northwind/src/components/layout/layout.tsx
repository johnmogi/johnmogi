import React, { Component } from "react";
import "./layout.css";
import { Header } from "../header/header";
import { Footer } from "../footer/footer";
import { Menu } from "../menu/menu";
import { Home } from "../home/home";

export class Layout extends Component {
    public render(): JSX.Element {
        return (
            <div className="layout">

                <header>
                    <Header />
                </header>

                <aside>
                    <Menu />
                </aside>

                <main>
                    <Home />
                </main>

                <footer>
                    <Footer />
                </footer>

            </div>
        );
    }
}